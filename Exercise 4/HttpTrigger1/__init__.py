import logging
import azure.functions as func
from math import sin

def intigration(lower : float, upper : float, j : list) -> list:

    ret = (upper - lower) / float(j)
    integral1 = 0.00
    i = 0

    while i < j:
        integral1+=(abs(sin((lower + i*ret)))*ret)
        i=1+i
    return integral1

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    lower = float(req.route_params.get("lower"))
    upper = float(req.route_params.get("upper"))
    N_list = [10, 100, 100, 1000, 10000, 100000, 1000000]
    results = []
    html = "<h3>Numerical Integral Service from {} to {} of abs(sin(x))</h3>".format(lower, upper)
    for i in N_list:
        results.append(intigration(lower, upper, i))
    html +="<h4>Iterations : Values</h4>"
    for i in range(len(results)):
        html+="<h4>{} : {}</h4>".format(N_list[i], results[i])
    


    
    return func.HttpResponse(
        html,
        status_code=200,
        mimetype="text/html"
    )
