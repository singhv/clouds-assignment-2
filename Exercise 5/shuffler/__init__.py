# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(input) -> list:
    ret = {}
    result = []
    for list in input:
        for pair in list:
            if pair[0] in ret:
                ret[pair[0]]+=1
            else:
                ret[pair[0]] = 1

    for key in ret.keys():
        value = [1 for _ in range(ret[key])]
        result.append([key, value])

    return result
