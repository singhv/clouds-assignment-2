# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    # Getting blob storage
    getDataResult = yield context.call_activity('GetInputDataFn', "verneetblob")

    # Maping jobs
    map = [context.call_activity('Mapper', input) for input in getDataResult]
    Mapping = yield context.task_all(map)

    # Shuffling Phase
    Shuffling = yield context.call_activity('Shuffler', Mapping)

    # Reducing Phase
    reduce = [context.call_activity('Reducer', pair) for pair in Shuffling]
    Reducing = yield context.task_all(reduce)

    Results = [
       
        {"type": "Mapping", "result": Mapping},
        {"type": "Shuffling", "result": Shuffling},
        {"type": "Reducing", "result": Reducing},
         {"type": "getData", "result": getDataResult}
        ]
    
    return Results

main = df.Orchestrator.create(orchestrator_function)
