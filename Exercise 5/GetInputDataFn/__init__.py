# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from azure.identity import DefaultAzureCredential

import os

from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient


def main(BlobName: str) -> list:
    # Connect to Blob
    connect = 'DefaultEndpointsProtocol=https;AccountName=reducemap;AccountKey=NmJWUqmLKf/NeeuMVcFXoTjsHOuJkzKh+vKdP2ESTwk1eW9buzoc5eVlVvgWa6RGE6LIQXTQ8xLb+ASth9v+KQ==;EndpointSuffix=core.windows.net'
    blob_service_client = BlobServiceClient.from_connection_string(connect)
    container_client = blob_service_client.get_container_client(container=BlobName)

    # Download all Files 
    file_array = []
    blob_list = container_client.list_blobs()
    for blob in blob_list:
        file_array.append(container_client.download_blob(blob.name).readall())

    index = 0
    data = []
    for i in file_array:
        decodedFile = i.decode()
    # Split by new line char
    for line in decodedFile.split("\n"):
        data.append({"key": index, "value": line})
        index+=1
    return data
